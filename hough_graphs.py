import numpy as np
import cv2
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec


img = cv2.imread("t.png", cv2.IMREAD_GRAYSCALE)

def hough_transform(img_bin, theta_res=1, rho_res=1):
    nR,nC = img_bin.shape #read the size of the image
    theta = np.linspace(-90.0, 0.0, np.ceil(90.0/theta_res) + 1.0)
    theta = np.concatenate((theta, -theta[len(theta)-2::-1]))
    D = np.sqrt((nR - 1)**2 + (nC - 1)**2)
    q = np.ceil(D/rho_res)
    nrho = 2*q + 1
    rho = np.linspace(-q*rho_res, q*rho_res, nrho)
    H = np.zeros((len(rho), len(theta)))

    for rowIdx in range(nR):
        for colIdx in range(nC):
            if img_bin[rowIdx, colIdx]:
                for thIdx in range(len(theta)):
                    rhoVal = colIdx*np.cos(theta[thIdx]*np.pi/180.0) + rowIdx*np.sin(theta[thIdx]*np.pi/180)
                    rhoIdx = np.nonzero(np.abs(rho-rhoVal) == np.min(np.abs(rho-rhoVal)))[0]
                    H[rhoIdx[0], thIdx] += 1
    return rho, theta, H

    
def make_ticklabels_invisible(fig):
    for i, ax in enumerate(fig.axes):
        #ax.text(0.5, 0.5, "ax%d" % (i+1), va="center", ha="center")
        for tl in ax.get_xticklabels() + ax.get_yticklabels():
            tl.set_visible(False)

def plotit(img, hough, zoom):
    f = plt.figure(figsize=(8,6))
    gs = GridSpec(3, 3)
    gs.update(hspace=0.1, wspace=0.1)
    
    ax1 = plt.subplot(gs[:2,0])
    ax1.imshow(hough)
    ax1.set_title("Hough space")
    
    ax2 = plt.subplot(gs[0,1:3])
    ax2.imshow(zoom)
    ax2.set_title("Hough space (zoom)")

    ax3 = plt.subplot(gs[1,1:3])
    ax3.imshow(img, cmap="binary")
    ax3.set_title("Image used")
    
    make_ticklabels_invisible(plt.gcf())
    #gs.tight_layout(f) #plt.tight_layout()
    plt.show()

r, t, h = hough_transform(img, theta_res=1, rho_res=1)
plotit(img, h[650:1200, :], h[750:820, 0:93]) #h[0:400, :600])
