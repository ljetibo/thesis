\contentsline {chapter}{\numberline {1}Introduction}{10}{chapter.1}
\contentsline {section}{\numberline {1.1}Meteors}{11}{section.1.1}
\contentsline {section}{\numberline {1.2}Introduction to SDSS}{13}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Camera}{13}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Run, camcol, filter, field}{15}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Data access, files and folder structure}{16}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Space and time constraint estimates}{18}{section.1.3}
\contentsline {section}{\numberline {1.4}Why Python?}{21}{section.1.4}
\contentsline {chapter}{\numberline {2}Linear Feature Detection Software}{23}{chapter.2}
\contentsline {section}{\numberline {2.1}Dependencies}{24}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Erin Sheldon's SDSSPY}{24}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Erin Sheldon's FITSIO}{26}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}OpenCV}{26}{subsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.3.1}Erosion and Dilatation}{27}{subsubsection.2.1.3.1}
\contentsline {subsubsection}{\numberline {2.1.3.2}Histogram equalization}{27}{subsubsection.2.1.3.2}
\contentsline {subsubsection}{\numberline {2.1.3.3}Canny edge detection}{27}{subsubsection.2.1.3.3}
\contentsline {subsubsection}{\numberline {2.1.3.4}Contours detection}{28}{subsubsection.2.1.3.4}
\contentsline {subsubsection}{\numberline {2.1.3.5}Minimal Area Rectangle}{28}{subsubsection.2.1.3.5}
\contentsline {subsubsection}{\numberline {2.1.3.6}Hough transform}{29}{subsubsection.2.1.3.6}
\contentsline {section}{\numberline {2.2}LFDS Modules}{32}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}analyse}{33}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}createjobs}{34}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}errors}{40}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}results}{41}{subsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.4.1}imagechecker}{44}{subsubsection.2.2.4.1}
\contentsline {subsection}{\numberline {2.2.5}detecttrails}{46}{subsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.5.1}processfield}{50}{subsubsection.2.2.5.1}
\contentsline {subsubsection}{\numberline {2.2.5.2}removestars}{62}{subsubsection.2.2.5.2}
\contentsline {section}{\numberline {2.3}LFDS recap and processing examples}{71}{section.2.3}
\contentsline {chapter}{\numberline {3}LFDS benchmarking}{76}{chapter.3}
\contentsline {section}{\numberline {3.1}Execution time}{76}{section.3.1}
\contentsline {section}{\numberline {3.2}Detection rates}{80}{section.3.2}
\contentsline {chapter}{\numberline {4}Conclusion}{82}{chapter.4}
\contentsline {chapter}{Bibliography}{83}{chapter.4}
